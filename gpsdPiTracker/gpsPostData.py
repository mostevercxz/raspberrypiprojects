#!/usr/bin/python -tt

import urllib
import urllib2, time
import wddx
from gpsLogger import gpslog

import ConfigParser

config = ConfigParser.RawConfigParser()
config.read('/etc/gpstrack.conf')
url = config.get("parms", "url")
sendmethod  = config.get("parms", "postmethod")

class postGPSData:
	
	def __init__(self, unit, gpsTime, gpsLat, gpsLong, gpsSpeed, gpsCourse):
		self.unit = unit
		self.gpsTime = gpsTime
		self.gpsLat = gpsLat
		self.gpsLong = gpsLong
		self.gpsSpeed = gpsSpeed
		self.gpsCourse = gpsCourse
		try:
			params = urllib.urlencode({
				'method': sendmethod,
				'unit': self.unit,
				'gpsTime': self.gpsTime,
				'gpsLat':	self.gpsLat,
				'gpsLong':self.gpsLong,
				'gpsSpeed':	self.gpsSpeed,
				'gpsCourse':self.gpsCourse
				})
			response = urllib2.urlopen(url, params).read()
			print response
			#test = wddx.loads(response)
			#print test[0]
		except Exception as err:
			pass
			print 'GPS POST ERROR: ' + str(err)
			gpslog('ERROR', 'GPS POST ERROR: ' + str(err))
			time.sleep(1)