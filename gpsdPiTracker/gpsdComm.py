#!/usr/bin/python -tt
#serialGetGPS.py

import serial, time, csv, sys, os
import gps

from gpsPostData import postGPSData
from datetime import datetime
from dateutil import tz

from gpsLogger import gpslog
from subprocess import *

PMTK_SET_NMEA_UPDATE_1HZ = '$PMTK220,1000*1F\r\n'
PMTK_SET_NMEA_UPDATE_5HZ = '$PMTK220,200*2C\r\n'
PMTK_SET_NMEA_UPDATE_10H = '$PMTK220,100*2F\r\n'
##turn on only the second sentence (GPRMC)
PMTK_SET_NMEA_OUTPUT_RMCONLY = '$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n'
##turn on GPRMC and GGA
PMTK_SET_NMEA_OUTPUT_RMCGGA = '$PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n'
##turn on ALL THE DATA
PMTK_SET_NMEA_OUTPUT_ALLDATA = '$PMTK314,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n'
## turn off output
PMTK_SET_NMEA_OUTPUT_OFF = '$PMTK314,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n'

class startGPSComm:
	
	def doSetup(self):
		rcode = call('sudo killall gpsd', shell=True)
		DEVICE = '/dev/ttyAMA0'
		BAUD = 115200
		
		ser = serial.Serial(DEVICE, BAUD)
		ser.open()
		ser.flush()
		##ser.writelines(PMTK_SET_NMEA_UPDATE_1HZ, PMTK_SET_NMEA_OUTPUT_RMCONLY)
		ser.write(PMTK_SET_NMEA_UPDATE_1HZ)
		ser.write(PMTK_SET_NMEA_OUTPUT_RMCONLY)
		##ser.write(PMTK_SET_NMEA_OUTPUT_RMCGGA)
		
		ser.close()
		return 1
	
	def doComm(self, gpsunit, updateint):
		self.unitNum = gpsunit
		self.updateinterval = int(updateint)
		
		time.sleep(2)
		rcode = call('sudo gpsd /dev/ttyAMA0 -F /var/run/gpsd.sock', shell=True )

		if rcode == 0:
			session = gps.gps("localhost", "2947")
			session.stream(gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)
			while True:	
				try:		
					report = session.next()		
					# Wait for a 'TPV' report and display the current time		
					# To see all report data, uncomment the line below		
					# print report		
					if report['class'] == 'TPV':
						print report
						#print report.time
						timeUTC = str(report.time).replace('T', ' ').replace('Z', '') #time.strftime("%H:%M:%S", time.strptime(report.time.split(".")[0],"%H%M%S"))
						
						mph = round(report.speed * gps.MPS_TO_MPH,2)
						gpsLat = report.lat
						gpsLong = report.lon
						gpsCourse = round(report.track)
						logme = "time:", timeUTC, "lat:", gpsLat, "long:", gpsLong, "speed:", mph, "course:", gpsCourse
						postGPSData('998', timeUTC, gpsLat, gpsLong, mph, gpsCourse)
						##gpstrack('INFO', logme)
						print logme
                        
						#if hasattr(report, 'time'):				
						#	print report.time
						#if hasattr(report, 'speed'):
						#	print report.speed * gps.MPS_TO_MPH
                        
				except KeyError:		
					pass	
				except KeyboardInterrupt:		
					quit()	
				except StopIteration:		
					session = None		
					print "GPSD has terminated"
                time.sleep(5)
                
    def _init_(self):
        ready=1
		