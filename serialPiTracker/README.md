This is to use UART serial communication to GPS.

It specifically utilizes commands for the Adafruti Ultimate GPS

It also has the ability to post parsed data diretly to a webservice
or use a UDP send to a server listening.

I will add a project for the UDP listener as well.

I use restartd to keep it running as well as a bash script to have it start at boot.
If interested in that I can send you the script, but I just jeyed it off the skeleton
in /etc/init.d

This will need pyserial as well as wddx for communication to WSDL.

http://learn.adafruit.com/adafruit-ultimate-gps-on-the-raspberry-pi

This is a great tutorial that came out long after I wrote the first serial code,

It gives the GPSD and UART configuration specs.  The code in my project is for the processing
from raw serial and now converted to GPSD which is much less intensive on processor but at
the same speed.  

You will also see a need for Configuration files and log files

Feel free to ask any questions
dnetman99@gmail.com