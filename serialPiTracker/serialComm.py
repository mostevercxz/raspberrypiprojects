#!/usr/bin/python -tt
#serialGetGPS.py

import serial, time, csv, sys
import parseNMEA
from gpsPostData import postGPSData
from datetime import datetime
from dateutil import tz
from gpsTrackLog import gpstrack
from gpsLogger import gpslog
from unitCallHome import callhome
from socketSend import sendUDP

class startSerialComm:
	def _init_(self):
		ready=1
	
	def startComm(self, gpsunit, updateint):
		##different commands to set the update rate from once a second (1 Hz) to 10 times a second (10Hz)
		PMTK_SET_NMEA_UPDATE_1HZ = '$PMTK220,1000*1F\r\n'
		PMTK_SET_NMEA_UPDATE_5HZ = '$PMTK220,200*2C\r\n'
		PMTK_SET_NMEA_UPDATE_10H = '$PMTK220,100*2F\r\n'
		self.unitNum = gpsunit
		self.updateinterval = int(updateint)
		##print unitNum
		##turn on only the second sentence (GPRMC)
		PMTK_SET_NMEA_OUTPUT_RMCONLY = '$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n'
		##turn on GPRMC and GGA
		PMTK_SET_NMEA_OUTPUT_RMCGGA = '$PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n'
		##turn on ALL THE DATA
		PMTK_SET_NMEA_OUTPUT_ALLDATA = '$PMTK314,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n'
		## turn off output
		PMTK_SET_NMEA_OUTPUT_OFF = '$PMTK314,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n'
		knot=float(1.15077945)
		
		DEVICE = '/dev/ttyAMA0'
		BAUD = 115200
		ser = serial.Serial(DEVICE, BAUD)
		ser.open()
		ser.flush()
		##ser.writelines(PMTK_SET_NMEA_UPDATE_1HZ, PMTK_SET_NMEA_OUTPUT_RMCONLY)
		ser.write(PMTK_SET_NMEA_UPDATE_1HZ)
		ser.write(PMTK_SET_NMEA_OUTPUT_RMCONLY)
		##ser.write(PMTK_SET_NMEA_OUTPUT_RMCGGA)
		
		ser.close()
		time.sleep(1)
		BAUD = 9600
		ser = serial.Serial(DEVICE, BAUD)
		ser.open()
		count = 0
		out = ''
		##callhome(trackUnit)
		while True:
			while ser.inWaiting() > 0:
				out = ser.readline()
				##	print "sats:" + str(numSats)
				if out.startswith("$GPRMC"):
					# Create the object
					count = count + 1
				
					if (count % self.updateinterval) == 0:
						
						try:
							sendUDP(self.unitNum, out)
						except Exception as err:
							gpslog('ERROR', err)
							pass