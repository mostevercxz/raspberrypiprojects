#!/usr/bin/python -tt

import socket
from gpsLogger import gpslog

import ConfigParser

config = ConfigParser.RawConfigParser()
config.read('/etc/gpstrack.conf')
#url = config.get("parms", "url")
IPADDR = config.get("parms", "host") #'gps1.abbadabbatech.com'
PORTNUM = config.get("parms", "port") #5000


class sendUDP:
	
	def __init__(self, devid, data):
		self.devid = '$DEVID,0x'+str(devid) + '*09\n'
		self.data = str(data)
		#gpslog('ERROR', 'DATA' + self.devid + ' ' + self.data)
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		PACKETDATA = self.devid + self.data
		SENDTOSERVER = (IPADDR,int(PORTNUM))
		#gpslog('INFO', SENDTOSERVER)
		try:
			
			#gpslog('ERROR', 'PD' + str(PACKETDATA))
			#NEWDATA = PACKETDATA.encode('hex')
			#gpslog('ERROR', NEWDATA)
			
			#s.connect((IPADDR, PORTNUM))
			sent = s.sendto(str(PACKETDATA), SENDTOSERVER)
			#s.send(PACKETDATA)
			
		except Exception as err:
			pass
			gpslog('ERROR', 'GPS PACKET ERROR: ' + str(err))
			
		s.close()