This creates a UDP listener at its core using Python.

I have the code just as in the GPS processors to run as a Daemon
and uses the start/stop Daemon scripts to start on boot and
restartd keeps it running in case of error.

The Listener then processes NMEA sentences data, parses and posts
data to a webservice.  It is expecting specific data and was modeled
around the data that the Sierra Wireless ALEOS device sends.

You will also see a need for Configuration files and log files

Feel free to ask any questions
dnetman99@gmail.com