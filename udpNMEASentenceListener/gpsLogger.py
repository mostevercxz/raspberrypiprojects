#!/usr/bin/python -tt
#gpslogger.py
import logging

logger = logging.getLogger('gpslogger')
hdlr = logging.FileHandler('/var/log/gpslogger.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)

class gpsservicelog:


	def __init__(self, logtype, message):
		self.logtype = logtype
		self.logmessage = message
		if self.logtype == 'ERROR':
			logger.error(self.logmessage)
		else:
			logger.info(self.logmessage)
