#!/usr/bin/python -tt

import urllib
import urllib2
import wddx
from gpsLogger import gpsservicelog
from gpsErrorLogger import gpserrorlog

import ConfigParser

config = ConfigParser.RawConfigParser()
config.read('/etc/gpstrack.conf')
url = config.get("parms", "url")

class postGPSData:

	def __init__(self, unit, gpsTime, gpsLat, gpsLong, gpsSpeed, gpsCourse):
		self.unit = unit
		self.gpsTime = gpsTime
		self.gpsLat = gpsLat
		self.gpsLong = gpsLong
		self.gpsSpeed = gpsSpeed
		self.gpsCourse = gpsCourse
		try:
			params = urllib.urlencode({
				'method': 'gpsDataNew',
				'unit': self.unit,
				'gpsTime': self.gpsTime,
				'gpsLat':	self.gpsLat,
				'gpsLong':self.gpsLong,
				'gpsSpeed':	self.gpsSpeed,
				'gpsCourse':self.gpsCourse
				})
			response = urllib2.urlopen(url, params).read()
			test = wddx.loads(response)
			print test[0]
		except urllib2.URLError as err:
			pass
			gpserrorlog('ERROR', 'GPS POST ERROR: ' + str(err.reason))
