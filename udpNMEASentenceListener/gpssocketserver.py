import SocketServer
import re
import time, csv, sys
import parseNMEA
from gpsPostData import postGPSData
from datetime import datetime
from dateutil import tz
#from gpsTrackLog import gpstrack
from gpsLogger import gpsservicelog
from gpsErrorLogger import gpserrorlog

class MyUDPHandler(SocketServer.BaseRequestHandler):
    """
    This class works similar to the TCP handler class, except that
    self.request consists of a pair of data and client socket, and since
    there is no connection the client address must be given explicitly
    when sending data back via sendto().
    """


    def handle(self):
        def foo(s, leader, trailer):
                end_of_leader = s.index(leader) + len(leader)
                start_of_trailer = s.index(trailer, end_of_leader)
                return s[end_of_leader:start_of_trailer]

    	def processData(h):
            gdata = h.split("\n")

            knot = float(1.15077945)
            rmcstatus = 'Z'
            fail = 0
            for line in gdata:
                if line.startswith("$DEVID"):
                    newline = foo(line, ",", "*")
                    unitnum = newline[2:]
                    #line = newline
                if line.startswith("$GPRMC"):
                    try:
                       # print line
                        parseNMEA.parseLine(line)
                        rmcstatus = parseNMEA.getField('RmcStatus')
                    except (AssertionError, ValueError, TypeError) as e:
                        fail = 1
                        gpserrorlog('ERROR', 'PARSE ERROR: '+ str(unitnum) +':\n' + str(line) + ':\n' + str(e))

            if fail == 0:
                if rmcstatus == 'A':
                    timeUTC = time.strftime("%H:%M:%S", time.strptime(parseNMEA.getField('UtcTime').split(".")[0],"%H%M%S"))
                    try:
    					speedGPS = parseNMEA.getField('SpeedOverGround')
                    except ValueError:
    					speedGPS = float(0)
    					##print "Error 2"
    					gpslog('ERROR', e)
                    mph = round(speedGPS*knot,2)
                    gpsLat = parseNMEA.getField('Latitude')
                    gpsLong = parseNMEA.getField('Longitude')
                    try:
                        gpsCourse = round(parseNMEA.getField('CourseOverGround'))
                    except (ValueError, TypeError) as e:
                        #print 'GPS Course Value', parseNMEA.getField('CourseOverGround')
                        gpsCourse = 999
                        #gpslog('ERROR', 'GPSCOURSE ERROR: ' + str(e))

                    line = "UNIT",unitnum, "time:", timeUTC, "lat:", gpsLat, "long:", gpsLong, "speed:", mph, "course:", gpsCourse

                    postGPSData(unitnum,timeUTC,gpsLat,gpsLong,mph,gpsCourse)
                    print line
                else:
                    gpserrorlog('ERROR', str(unitnum)+': Bad RMS Status:'+str(rmcstatus))

				##gpstrack('INFO', logme)


        data = self.request[0].strip()
        mysocket = self.request[1]
        print "{} wrote:".format(self.client_address[0])
        #data2 = data.split("\n")
        print data
        processData(data)

        #print str(data)
        #socket.sendto(data.upper(), self.client_address)
