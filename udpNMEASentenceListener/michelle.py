import socket
import SocketServer
from gpssocketserver import MyUDPHandler


def myUDPServer(listenip, listenport):

        address = (str(listenip), listenport)
        #HOST, PORT = "108.166.178.166", 5000
        server = SocketServer.UDPServer(address, MyUDPHandler)
        server.serve_forever()